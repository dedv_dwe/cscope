Name: cscope
Summary: a developer's tool for browsing source code
Version: 15.9
Release: 5
Source0: https://downloads.sourceforge.net/project/%{name}/%{name}/v%{version}/%{name}-%{version}.tar.gz
URL: http://cscope.sourceforge.net
License: BSD and GPLv2+

BuildRequires: pkgconfig ncurses-devel flex bison m4 gcc autoconf automake

Requires: emacs-filesystem

%description
Cscope is a developer's tool for browsing source code. It has an impeccable
Unix pedigree, having been originally developed at Bell Labs back in the days
of the PDP-11. Cscope was part of the official AT&T Unix distribution for many
years, and has been used to manage projects involving 20 million lines of code

%package_help

%prep
%autosetup -p1

autoreconf

%build
%configure
%make_build

%install

%make_install
install -d %{buildroot}/var/lib/cs
install -d %{buildroot}%{_datadir}/%{name}
cp -a contrib/xcscope/xcscope.el %{buildroot}%{_datadir}/%{name}
install -m 755 contrib/xcscope/%{name}-indexer %{buildroot}%{_bindir}
cp -a contrib/cctree.vim %{buildroot}%{_datadir}/%{name}
for dir in %{_datadir}/xemacs/site-packages/lisp %{_datadir}/emacs/site-lisp ; do
  install -d %{buildroot}$dir
  ln -s %{_datadir}/%{name}/xcscope.el %{buildroot}$dir
  touch %{buildroot}$dir/xcscope.elc
  echo "%ghost $dir/xcscope.el*" >> %{name}-%{version}.files
done

%files -f %{name}-%{version}.files
%doc COPYING
%{_bindir}/*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/
%dir /var/lib/cs

%files help
%{_mandir}/man1/*
%doc AUTHORS ChangeLog TODO README
%doc contrib/cctree.txt

%triggerin -- emacs, emacs-nox, emacs-lucid
ln -sf %{_datadir}/%{name}/xcscope.el %{_datadir}/emacs/site-lisp/xcscope.el

%triggerun -- emacs, emacs-nox, emacs-lucid
if [ $2 -gt 0 ]
then
exit 0
fi
rm -f %{_datadir}/emacs/site-lisp/xcscope.el

%triggerin -- vim-filesystem
ln -sf %{_datadir}/%{name}/cctree.vim %{_datadir}/vim/vimfiles/plugin/cctree.vim

%triggerun -- vim-filesystem
if [ $2 -gt 0 ]
then
exit 0
fi
rm -f %{_datadir}/vim/vimfiles/plugin/cctree.vim

%changelog
* Mon Mar 15 2021 liudabo<liudabo1@h-partners.com> - 15.9-5
- split help package from main package

* Fri Dec 11 2020 shixuantong<shixuantong@huawei.com> - 15.9-4
- update Source0 and tarball from upstream

* Tue Dec 3 2019 caomeng<caomeng5@huawei.com> - 15.9-3
- Package init
